#!/bin/sh

set -e

readonly version="0.39.0"
readonly sha256sum="3e6e799c5c87e78abae791c331f4001d7671b7aaec67156cda031eaf8ee5c5d4"
readonly filename="cargo-semver-checks-x86_64-unknown-linux-musl.tar.gz"

cd .gitlab

echo "$sha256sum  $filename" > cargo-semver-checks.sha256sum
curl -OL "https://github.com/obi1kenobi/cargo-semver-checks/releases/download/v$version/$filename"
sha256sum --check cargo-semver-checks.sha256sum
tar -xf "$filename"
