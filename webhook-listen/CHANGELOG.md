# v1.2.2

  * CI fixes for publishing.

# v1.2.1

  * MSRV bumped to 1.60 due to dependencies.
  * CI updates to publish from CI.

# v1.2.0

  * MSRV bumped to 1.46. Dependencies have been updated to use the new `async`
    API within the `webhook-crate` itself as well.

# v1.1.0

  * Verification of received webhooks is now supported via either token or
    HMAC.
  * Header names are compared case-insensitively. See [RFC 2616][].
  * Data objects which contain a non-string in the lookup value can now be used
    to look up secrets.
  * `use_header_value` may now be used to replace `HEADER` in `kind` with the
    value of an HTTP header.
  * The `handlers` configuration key has been renamed to `post_paths`.
  * Logging backends may now be configured at build time. The default logger is
    `env_logger`, but `journald` logging may be enabled with the `systemd`
    feature. Logging to `sentry.io` is available via the `sentry` feature.
  * `secret_key_lookup` may be a non-JSON pointer (i.e., not start with `/`)
    in order to have a static secret for a given endpoint.

[RFC 2616]: https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2

# v1.0.3

  * Update the `systemd` dependency for improved logging.

# v1.0.2

  * Update dependencies.

# v1.0.1

  * Metadata updates.
  * Improved logging output.

# v1.0.0

  * Initial stable release.
