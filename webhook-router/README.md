# webhook-router

Verify, classify and route webhook events to files in directories based on the
headers and contents of the hooks.
