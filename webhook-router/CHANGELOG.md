# v0.2.2

  * CI fixes for publishing.

# v0.2.1

  * MSRV bumped to 1.60 due to dependencies.
  * CI updates to publish from CI.

# v0.2.0

  * MSRV bumped to 1.42. While the code update is just for a small code
    simplification, in order to simplify CI, updating makes sense.
  * `http` dependency bumped to 0.2.

# v0.1.0

  * Split from the `webhook-listen` executable crate.
